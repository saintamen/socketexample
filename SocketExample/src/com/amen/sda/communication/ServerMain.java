package com.amen.sda.communication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {

	public static void main(String[] args) {
		try{
		ServerSocket socketPolaczenia = new ServerSocket(10001);
		Socket otwartySocket = socketPolaczenia.accept();

		InputStream kanalWejscia = otwartySocket.getInputStream();
//		OutputStream kanalWyjscia = otwartySocket.getOutputStream()();
//		BufferedReader reader = new BufferedReader(new FileReader(new File("sciezka")));
		BufferedReader reader = new BufferedReader(new InputStreamReader(kanalWejscia));
		String otrzymanaLinia = reader.readLine();
		
		System.out.println(otrzymanaLinia);
		reader.close();
		}catch(IOException ioe){
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}

}
