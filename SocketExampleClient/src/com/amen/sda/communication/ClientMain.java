package com.amen.sda.communication;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientMain {
	public static void main(String[] args) {
		// 127.0.0.1
		try {
			Socket socket = new Socket("localhost", 10001);
			
			OutputStream os = socket.getOutputStream();
			
			PrintWriter writer = new PrintWriter(os);
			writer.println("linia tekstu");
			writer.flush();
			
			writer.close();
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}
}
